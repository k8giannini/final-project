//
//  Checkers_Classes.hpp
//  Final Project
//
//  Created by Kate Giannini on 1/18/17.
//  Copyright © 2017 Kate Giannini. All rights reserved.
//

#ifndef Checkers_Classes_hpp
#define Checkers_Classes_hpp

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class location;
class board;
class chip;
class player;
class human;
class computer;


/*******************************************************************************
 
                            LOCATION INTERFACE
 
 ******************************************************************************/

class location{
public:
    
    //friend classes
    friend class board;
    friend class player;
    friend class human;
    friend class computer;
    
    //constructors, destructor, assignment operator
    location();
    location(int row, int column);
    location(int index);
    location(const location& l);
    location& operator=(location l);
    virtual ~location();
    void swap(location& l);
    
    //getters
    int get_Up();
    int get_Down();
    int get_Left();
    int get_Right();
    int get_upLeft();
    int get_upRight();
    int get_downLeft();
    int get_downRight();
    
protected:
    
    //members
    int row;
    int column;
    int index;
    int Up;
    int Down;
    int Right;
    int Left;
    int upRight;
    int upLeft;
    int downRight;
    int downLeft;
    
};

/*******************************************************************************
 
                                CHIP INTERFACE
 
 ******************************************************************************/
//inherits from locations
class chip: public location {
public:
    //friend classes
    friend class board;
    friend class player;
    friend class human;
    friend class computer;
    
    //constructors, destructor, assignment operator
    chip();
    chip(int row, int column);
    chip(int index);
    chip(const chip& c);
    chip& operator=(chip c);
    void swap(chip& c);
    virtual ~chip();
    
    //comparison operators
    bool operator==(const chip& c) const;
    bool operator!=(const chip& c) const;
    
    //chip functions
    void swapStatus(chip& c);
    void setKing(bool b);
    bool colorIsSet() const;
    void setColor(const char& color);
    char getColor() const;
    
private:
    //memebrs
    char color;
    bool king;
    
};

/*******************************************************************************
 
                            BOARD INTERFACE
 
 ******************************************************************************/

class board{
public:
    //friend classes
    friend class location;
    friend class chip;
    friend class player;
    friend class human;
    friend class computer;
    
    //constructors, destructor, assignment operator
    board();
    board(const board& b);
    board& operator=(board b);
    void swap(board& b);
    virtual ~board();
    
    //chip access
    chip findLocation(int row, int column) const;
    chip& getChip(int index);
    
    //game development
    bool checkChips() const;
    void setKings();
    
    //display functions
    void displayChip1(chip) const;
    void displayChip2(chip) const;
    void displayChip3(chip) const;
    void display() const;
    
    
    
private:
    //board
    vector<chip> b;
    
};

/*******************************************************************************
 
                            PLAYER INTERFACE
 
 ******************************************************************************/
//abstract class
class player{
public:
    //friend classes
    friend class location;
    friend class chip;
    friend class board;
    friend class human;
    friend class computer;
    
    //constructor, destructor
    player(string name = "", char color = 'N');
    virtual ~player();
    
    //virtual functions
    virtual void play(board& b) = 0;
    virtual bool move(int start, int end, board& b) = 0;
    
protected:
    //members
    char color;
    string name;
};


/*******************************************************************************
 
                            OPTIONS INTERFACE
 
 ******************************************************************************/
class options{
public:
    //constructors, destructors
    options(){piece = 0; move = 0;}
    options(int p, int m){piece = p; move = m;}
    virtual ~options(){/* no dynamic memory*/}
    
    //members
    int piece;
    int move;
};

/*******************************************************************************
 
                            HUMAN INTERFACE
 
 ******************************************************************************/
//inherits from player
class human: public player{
public:
    //friend classes
    friend class location;
    friend class chip;
    friend class board;
    friend class computer;
    
    //constructor, destructor
    human(string name = "", char color = 'N');
    virtual ~human();
    
    //functions
    int isJump(int start, int end, board& b) const;
    void play(board& b);
    bool move(int start, int end, board& b);
};


/*******************************************************************************
 
                            COMPUTER INTERFACE
 
 ******************************************************************************/
class computer: public player{
public:
    //friend classes
    friend class location;
    friend class chip;
    friend class board;
    friend class computer;
    
    //constructor, destructor
    computer(string name = "", char color = 'N');
    virtual ~computer();
    
    //funcitons
    int isJump(int start, int end, board& b) const;
    void setOptions(const board& b);
    bool checkEdgeCases(int start, int end, const board& b);
    void play(board& b);
    bool move(int start, int end, board& b);
    
private:
    
    //members
    vector<options> moves;

};


#endif /* Checkers_Classes_hpp */
