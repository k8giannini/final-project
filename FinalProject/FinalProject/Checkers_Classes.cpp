//
//  Checkers_Classes.cpp
//  Final Project
//
//  Created by Kate Giannini on 1/18/17.
//  Copyright © 2017 Kate Giannini. All rights reserved.
//

#include "Checkers_Classes.hpp"
#include <iomanip>
#include <cstdlib>
#include <time.h>
using namespace std;

class chip;
class location;
class board;
class player;
class human;
class computer;


//Dimentions of a checkerboard
const int ROWS = 8;
const int COL = 8;

/******************************************************************************
 
                        LOCATION IMPLEMENTATION
 
 *****************************************************************************/

//Default constructor
location::location(){
    row = 0;
    column = 0;
    
    index = 0;
    Up = 0;
    Down = 0;
    Left = 0;
    Right = 0;
    upRight = 0;
    upLeft = 0;
    downRight = 0;
    downLeft = 0;
}

//Parameter constructor with a row and column
location::location(int r, int c){
    row = r;
    column = c;
    
    //initial
    index = -1;
    Up = -1;
    Down = -1;
    Left = -1;
    Right = -1;
    upRight = -1;
    upLeft = -1;
    downRight = -1;
    downLeft = -1;
    
    //check if there are valid locations adjacent
    if(r >= 0 && r < 8 && c >= 0 && c <8){index = r*ROWS + c;}
    if(r-1 >= 0){Up = (r-1)*ROWS + c;}
    if(r+1 < 8){Down = (r+1) + c;}
    if(c-1 >=0){Left = r*ROWS + (c-1);}
    if(c+1 < 8){Right = r*ROWS + (c+1);}
    if(r-1 >= 0 && c+1 < 8){upRight = (r-1)*ROWS + (c+1);}
    if(r-1 >= 0 && c-1 >=0){upLeft = (r-1)*ROWS + (c-1);}
    if(r+1 < 8 && c+1 < 8){downRight = (r+1)*ROWS + (c+1);}
    if(r+1 < 8 && c-1 >=0){downLeft = (r+1)*ROWS + (c-1);}
    
    
}

//parameter constructor that takes an index
location::location(int boardindex){
    
    int r = -1;
    int c = -1;
    index = -1;
    
    //check if index is valid
    if(boardindex >= 0 && boardindex < 64){
        index = boardindex;
    }
    
    //determines row and column based on board index
    if(boardindex >= 0 && boardindex < 8){
        r = 0;
        c = boardindex;
    }else if(boardindex >= 8 && boardindex < 16){
        r = 1;
        c = boardindex - 8;
    }else if (boardindex >= 16 && boardindex < 24 ){
        r = 2;
        c = boardindex - 16;
    }else if (boardindex >= 24 && boardindex < 32 ){
        r = 3;
        c = boardindex - 24;
    }else if (boardindex >= 32 && boardindex < 40 ){
        r = 4;
        c = boardindex - 32;
    }else if (boardindex >= 40 && boardindex < 48 ){
        r = 5;
        c = boardindex - 40;
    }else if (boardindex >= 48 && boardindex < 56 ){
        r = 6;
        c = boardindex - 48;
    }else if (boardindex >= 56 && boardindex < 64 ){
        r = 7;
        c = boardindex - 56;
    }
    
    row = r;
    column = c;
    
    Up = -1;
    Down = -1;
    Left = -1;
    Right = -1;
    upRight = -1;
    upLeft = -1;
    downRight = -1;
    downLeft = -1;
    
    //checks if there are valid adjacent locations
    if(r-1 >= 0){Up = (r-1)*ROWS + c;}
    if(r+1 < 8){Down = (r+1) + c;}
    if(c-1 >=0){Left = r*ROWS + (c-1);}
    if(c+1 < 8){Right = r*ROWS + (c+1);}
    if(r-1 >= 0 && c+1 < 8){upRight = (r-1)*ROWS + (c+1);}
    if(r-1 >= 0 && c-1 >=0){upLeft = (r-1)*ROWS + (c-1);}
    if(r+1 < 8 && c+1 < 8){downRight = (r+1)*ROWS + (c+1);}
    if(r+1 < 8 && c-1 >=0){downLeft = (r+1)*ROWS + (c-1);}
    
}

//destructor, there is no dynamic memory
location::~location(){
    //no dynamic memeory
}


// copy constructor
location::location(const location& l){
    
    //no dynamic memeory so just set values
    
    row = l.row;
    column = l.column;
    index = l.index;
    
    Up = l.Up;
    Down = l.Down;
    Left = l.Left;
    Right = l.Right;
    upLeft = l.upLeft;
    upRight = l.upRight;
    downLeft = l.downLeft;
    downRight = l.downRight;
}

//swap function
void location::swap(location& l){
    
    //swap all memebers
    std::swap(index, l.index);
    std::swap(row, l.row);
    std::swap(column, l.column);
    std::swap(Up, l.Up);
    std::swap(Down, l.Down);
    std::swap(Right, l.Right);
    std::swap(Left, l.Left);
    std::swap(upRight, l.upRight);
    std::swap(upLeft, l.upLeft);
    std::swap(downRight, l.downRight);
    std::swap(downLeft, l.downLeft);
}

//assignment operator (copy and swap)
location& location::operator=(location l){
    this->swap(l);
    return *this;
}

//getters of memeber functions
int location::get_Up(){
    return Up;
}

int location::get_Down(){
    return Down;
}

int location::get_Left(){
    return Left;
}

int location::get_Right(){
    return Right;
}

int location::get_upLeft(){
    return upLeft;
}

int location::get_upRight(){
    return upRight;
}

int location::get_downLeft(){
    return downLeft;
}

int location::get_downRight(){
    return downRight;
}

/******************************************************************************
 
                            BOARD IMPLEMENTATION
 
 *****************************************************************************/

//default constructor
board::board(){
    
    //try catch if push_back fails
    try{
        
        int counter = 0;
        
        //set up chips
        while(b.size()< ROWS*COL){
            b.push_back(chip(counter++));
        }
        
    }
    catch(exception& e){
        
        cout << "exception";
        
        throw;
        
    }
    
}

//copy constructor
board::board(const board& brd){
    //no dynamic memory so just assign
    b = brd.b;
}

//swap function
void board::swap(board& brd){
    std::swap(b,brd.b);
}

//assignment operator (copy and swap)
board& board::operator=(board brd){
    this->swap(brd);
    return *this;
}

//destructor, no dynamic memeory
board::~board(){
    //no dynamic memeory
}

/*
 setKings()
 param: void
 use: checks first and last row for black and red chips respectivly 
    to set the corresponding chips member king = true to allow for more
    move funtionality
 */
void board::setKings(){
    
    //loop through first row
    for(int i = 0; i < 8; ++i){
        if(b[i].color == 'b'){
            b[i].king = true;
        }
    }
    
    //loop through last row
    for(int i = 56; i < 64; ++i){
        if(b[i].color == 'r'){
            b[i].king = true;
        }
    }
}

/*
 findLocation(int row, int column)
 param: two ints
 use: if the row and column entered corresponds to a valid space on the board
    it will return the chip at that given location by copy to insure no motification
    to the board
 
 */
chip board::findLocation(int row, int column) const {
    if(row >= ROWS || row < 0 || column >= COL || column < 0){
        cout << "Not a valid location" << endl;
        //if not a valid location it will return the default chip
        return chip();
    }
    
    return b[row*ROWS + column];
}

/*
 checkChips()
 param: void
 use: goes through board to check if there is at least one black chip and 
    one red chip left, if so it will return true, if not it will return false
    used to check if the game should continue
 */
bool board::checkChips() const{
    bool red = false;
    bool black = false;
    
    for(auto c: b){
        //check for red chip
        if(c.getColor() == 'r'){
            red = true;
        }
        //check for black chip
        if(c.getColor() == 'b'){
            black = true;
        }
        //check if there is at least one chip of each color
        if(red && black){
            return true;
        }
    }
    
    return false;
}

/*
 displayChip1(chip)
 param: chip
 use: for display of board there are three lines for each cell so this one 
    corresponds to the first line, display based on chip color and if the chip
    if a king or not
    example output: 
    for red chip non-King, for black chip non king, Red and black kings
             R                        B              KNG     KNG  <- this line corresponds
            RED                      BLK             RED     BLK     to displayChip1(chip)
             R                        B              KNG     KNG
 
 */
void board::displayChip1(chip c) const{
    
    /*
     none are centered because in the top left corner of each cell is where
     the cell number is displayed so the design of the chip is shifted accordingly
     */
    
    //first check for king
    if(c.king){
        cout << "KNG  ";
        return;
    }
    
    //now determine for color if not king
    char color = c.getColor();
    switch(color){
        case 'r':
            cout << " R   ";
            break;
        case 'b':
            cout << " B   ";
            break;
        case 'N':
            cout << "     ";
            break;
        default:
            cout << "     ";
    }
}
/*
displayChip2(chip)
param: chip
use: for display of board there are three lines for each cell so this one
corresponds to the second line, display based on chip color
example output:
for red chip non-King, for black chip non king, Red and black kings
         R                        B              KNG     KNG       this line corresponds
        RED                      BLK             RED     BLK    <- to displayChip2(chip)
         R                        B              KNG     KNG

*/
void board::displayChip2(chip c) const{
    
    //switch statement based on color
    char color = c.getColor();
    switch(color){
        case 'r':
            cout << "  RED  ";
            break;
        case 'b':
            cout << "  BLK  ";
            break;
        case 'N':
            cout << "       ";
            break;
        default:
            cout << "       ";
    }
}

/*
 displayChip3(chip)
 param: chip
 use: for display of board there are three lines for each cell so this one
 corresponds to the third line, display based on chip color and if the chip
 if a king or not
 example output:
 for red chip non-King, for black chip non king, Red and black kings
          R                        B              KNG     KNG
         RED                      BLK             RED     BLK
          R                        B              KNG     KNG   <- this line corresponds
                                                                   to displayChip1(chip)
 
 */
void board::displayChip3(chip c) const{
    
    //check if chip is a king
    if(c.king){
        cout << "  KNG  ";
        return;
    }
    
    /*
     different from displayChip1 becasue the design of the chip is not shifted
     because the cell number is not displayed at the bottom of the cell
     */
    
    //switch statement based on color if not king
    char color = c.getColor();
    switch(color){
        case 'r':
            cout << "   R   ";
            break;
        case 'b':
            cout << "   B   ";
            break;
        case 'N':
            cout << "       ";
            break;
        default:
            cout << "       ";
    }
}

/*
 display()
 param: void
 use: after each play the board can be displayed of the new layout of the chips
 
 */
void board::display() const{
    int rowCount = 0;
    int columnCount = 0;
    int cellNum = 1;
    
    //top boarder
    cout << "* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * " << endl;
    
    while(rowCount < ROWS){
        //display the first line of the cell
        while(columnCount < COL){
            cout << "*" << setw(2) << left << cellNum;
            displayChip1(findLocation(rowCount, columnCount));
            ++cellNum;
            ++columnCount;
        }
        
        //end cell
        cout << "*" << endl;
        columnCount = 0;
        
        //diplay the second line of the cell
        while(columnCount < COL){
            cout << "*";
            displayChip2(findLocation(rowCount, columnCount));
            ++columnCount;
        }
         //end cell
        cout << "*" << endl;
        columnCount = 0;
        
        //display the third line of the cell
        while(columnCount < COL){
            cout << "*";
            displayChip3(findLocation(rowCount, columnCount));
            ++columnCount;
        }
        
        //end cell
        cout << "*" << endl;
        columnCount = 0;
        
        //line divider
        cout << "* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * " << endl;
        
        //move to the next row
        ++rowCount;
    }

    
}

/*
 getChip(int index)
 param: int
 use: if int corresponds to a valid index in the board then return the chip that 
    corresponds to that index
 */
chip& board::getChip(int index){
    //check if valid index
    if(index >= 0 && index < 64){
        return b[index];
    }
    
    //if not valid return first position
    return b[0];
}


/******************************************************************************
 
                            CHIP IMPLEMENTATION
 
 *****************************************************************************/

//default constructor
chip::chip():location(){
    //no dynamic memeory so no try catch
    color = 'N';
    king = false;
}

//param consturctor takes in row and column
chip::chip(int row, int column): location(row,column){
    color = 'N';
    king = false;
}

//param constructor that takes in an index
chip::chip(int index):location(index){
    color = 'N';
    king = false;
    
}

//copy consturctor
chip::chip(const chip& c){
    
    //no dynamic memory so just assign
    color = c.color;
    king = c.king;
    
    row = c.row;
    column = c.column;
    index = c.index;
    
    Up = c.Up;
    Down = c.Down;
    Left = c.Left;
    Right = c.Right;
    upLeft = c.upLeft;
    upRight = c.upRight;
    downLeft = c.downLeft;
    downRight = c.downRight;
    
}

//assignment operator (copy and swap)
chip& chip::operator=(chip c){
    swap(c);
    return *this;
}

// operator==
bool chip::operator==(const chip &c) const{
    if(*this != c){
        return false;
    }
    return true;
}

//operator !=
bool chip:: operator!=(const chip& c) const{
    if(index != c.index || row!= c.row || column != c.column || Up != c.Up ||
       Down != c.Down || Right != c.Right || Left != c.Left || upRight != c.upRight
       || upLeft != c.upLeft || downRight != c.downRight || downLeft != c.downLeft
       || color != c.color || king != c.king){
        return true;
    }
    
    return false;
    
}

//destructor
chip::~chip(){
    //mananges no memeory directly so let location do its thing
}

/*
 setColor(const char& c)
 param: const char&
 use: to set the color of the chip
 */
void chip::setColor(const char& c){
    color = c;
}


/*
 setKing(bool b)
 param: bool
 use: set king to true or false
 */
void chip::setKing(bool b){
    king = b;
}


/*
 swapStatus(chip& c)
 param: chip&
 use: will swap the colors of the implicit chip and the chip passed into the 
    function and will swap their king status.
    different from swap because it maintains location
 */
void chip::swapStatus(chip& c){
    
    //swap the colors
    char temp = color;
    color = c.color;
    c.color = temp;
    
    //swap the king status
    bool b = king;
    king = c.king;
    c.king = b;
}


/*
 getColor()
 param: void
 use: returns the color of the implicit chip
 */
char chip::getColor() const{
    return color;
}

/*
 swap(chip)
 param: chip
 use: swap all member variables, used in copy and swap
 */
void chip::swap(chip& c){
    std::swap(index,c.index);
    std::swap(color, c.color);
    std::swap(row, c.row);
    std::swap(column, c.column);
    std::swap(upLeft,c.upLeft);
    std::swap(upRight, c.upRight);
    std::swap(downLeft, c.downLeft);
    std::swap(downRight,c.downRight);
    std::swap(Up,c.Up);
    std::swap(Down, c.Down);
    std::swap(Right, c.Right);
    std::swap(Left, c.Left);
}

/*
 colorIsSet()
 param: void
 use: color is either 'N', 'r', or 'b' so whe color is set to 'N' it will return
    false, otherwise it will return true
 */
bool chip::colorIsSet() const{
    if(color == 'N'){
        return false;
    }
    
    return true;
}

/******************************************************************************
 
                            PLAYER IMPLEMENTATION
 
 *****************************************************************************/

//default and param constructor, (default parameters)
player::player(string n, char c){
    //set color and name
    color = c;
    name = n;
}

//destuctor
player::~player(){
    //no heap memory
}



/******************************************************************************
 
                            HUMAN IMPLEMENTATION
 
 *****************************************************************************/

//default and parameter constuctor (default parameters)
human::human(string n, char c):player(n,c){
    //nothing to set, no memebers in human
}

//destructor
human::~human(){
    //nothing to do
}

/*
 isJump(int, int, board&)
 param: int , int , board&
 use: the first int signifies the starting cell and the second int refers to the
    ending position of where they want the chip at the starting cell to end up
    with this information the function determines whether or not a jump has occurred
    (it does not check if the jump is valid) jump is defined as moving two rows away
    and two columns away from the start. This can be determined by the cell number
    becasuse the board is a vector with the rows next to eachother
    picture:
    { row1, row2, row3 , ...., row8}
 
 
 */
int human::isJump(int start, int end, board& b) const{
    
    //first determine that it is not just one row and column away
    if(end > (start - 10) && end < (start + 10)){
        return -1;
    }
    
    //if end > start it is below the start
    if(end > start){
        //down left
        if(end == start+14){
            return 3;
        }
        
        //down right
        if(end == start+18){
            return 4;
        }
        
        return -1;
    }
    
    // if end < start  it is above start
    if(end < start){
        //up right
        if(end == start-14){
            return 2;
        }
        
        //up left
        if(end == start-18){
            return 1;
        }
        
        return -1;
    }
    
    //bad bad bad
    if(end == start){
        return -1;
    }
    
    //return if not a jump or invalid move
    return -1;
}

/*
 move(int,int,board&)
 param: int, int, board&
 use: will swap the colors of the chips at the corresponding locations to the
 two int inputs on the board, assumes move is valid
 
 */
bool human::move(int start, int end, board &b){
    b.b[start].swapStatus(b.b[end]);
    return true;
}

/*
 play(board&)
 param: board&
 use: to prompt the human user to enter thier move and then execute the move 
    given that they have chosen a chip that is of their color and a destination
    of no color, program assumes that user is playing by the rules
 */
void human::play(board& b){
    int cell = -1;
    int end = -1;
    
    cout << name << " it is your turn, enter move: "<< endl;
    cin >> cell;
    cin >> end;
    
    //cell and end entered will be one more than we want because cell numbers
    //start at 1 as opposed to zero
    cell--;
    end--;
    
    //check for valid chip and move
    while(b.getChip(cell).getColor() != color || b.getChip(end).getColor() != 'N'){
        
        if(b.getChip(cell).getColor() != color ){
            cout << "Not your color, please pick one of your chips to move" << endl;
        }
        
        if(b.getChip(end).getColor() != 'N'){
            cout << "invalid destination, please pick again"<< endl;
        }
        
        cout << "Enter move: " << endl;
        cout << "Example: 2 11, moves chip at location 2 -> location 11" << endl;
        
        cin >> cell;
        cin >> end;
        
        cell--;
        end--;
    }
    
    //clean up chip that has been jumped if a jump has occured
    int jump = isJump(cell, end, b);
    
    if(jump > 0){
        switch (jump){
            case 1: b.b[cell-9].setColor('N');
                b.b[cell-9].setKing(false);
                break;
            case 2: b.b[cell-7].setColor('N');
                b.b[cell-7].setKing(false);
                break;
            case 3: b.b[cell+7].setColor('N');
                b.b[cell+7].setKing(false);
                break;
            case 4: b.b[cell+9].setColor('N');
                b.b[cell+9].setKing(false);
                break;
        }
    }
    
    //swap the colors
    move(cell, end, b);

}

/******************************************************************************
 
                        COMPUTER IMPLEMENTATION
 
 *****************************************************************************/

//default and parameter constructor (default parameters)
computer::computer(string n, char c):player(n,c){
    //nothing to set, because vector has its own default constructor
}

//destructor
computer::~computer(){
    //nothing to do, no heap memory
}

/*
 isJump(int, int, board&)
 param: int , int , board&
 use: the first int signifies the starting cell and the second int refers to the
 ending position of where they want the chip at the starting cell to end up
 with this information the function determines whether or not a jump has occurred
 (it does not check if the jump is valid) jump is defined as moving two rows away
 and two columns away from the start. This can be determined by the cell number
 becasuse the board is a vector with the rows next to eachother
 picture:
 { row1, row2, row3 , ...., row8}
 
 */
int computer::isJump(int start, int end, board &b) const{
    
    //to determine if it is under two rows and two columns away
    if(end > (start - 10) && end < (start + 10)){
        return -1;
    }
    
    //if end > start then end is below start
    if(end > start){
        //down left
        if(end == start+14){
            return 3;
        }
        
        //down right
        if(end == start+18){
            return 4;
        }
        
        //invalid
        return -1;
    }
    
    //if end < start then end is above start
    if(end < start){
        
        //up right
        if(end == start-14){
            return 2;
        }
        
        //up left
        if(end == start-18){
            return 1;
        }
        
        //invalid
        return -1;
    }
    
    //bad bad bad
    if(end == start){
        return -1;
    }
    
    //invalid or just not a jump
    return -1;
}

/*
 play(board&)
 param: board&
 use: for the program to determine all the valid moves it can make, then randomly
    choose which to make
 */
void computer::play(board& b){
    //setting all possible options
    setOptions(b);
    
    //random seed
    srand(time(0));
    //index that will determine what move to make
    int index = rand()%(moves.size());
    
    //determines if a jump has occured
    int jump = isJump(moves[index].piece, moves[index].move, b);
    
    //clean up if a jump has occcured
    if(jump > 0){
        switch (jump){
            case 1: b.b[moves[index].piece-9].setColor('N');
                b.b[moves[index].piece-9].setKing(false);
                break;
            case 2: b.b[moves[index].piece-7].setColor('N');
                b.b[moves[index].piece-7].setKing(false);
                break;
            case 3: b.b[moves[index].piece+7].setColor('N');
                b.b[moves[index].piece+7].setKing(false);
                break;
            case 4: b.b[moves[index].piece+9].setColor('N');
                b.b[moves[index].piece+9].setKing(false);
                break;
        }
    }
    
    //execute the the move selected
    move(moves[index].piece, moves[index].move , b);
}


/*
 move(int,int,board&)
 param: int, int, board&
 use: will swap the colors of the chips at the corresponding locations to the 
 two int inputs on the board, assumes move is valid
 
 */
bool computer::move(int start, int end, board& b){
    b.b[start].swapStatus(b.b[end]);
    return true;
}

/*
 checkEdgeCases(int,int,board&)
 param: int , int , board&
 use: the first int refers the the index of the chip that is to move to second int 
    which is the index of the ending chip. it will check it the move is valid assuming
    that the chip is the correct color and the ending space is available
 */
bool computer::checkEdgeCases(int start, int end, const board &b){
    //start row, start column
    int sr = -1, sc = -1;
    //end row, end column
    int er = -1, ec = -1;
    
    //setting sr and sc
    if(start >= 0 && start < 8){sr = 0;sc = start;}
    else if(start >= 8 && start < 16){sr = 1;sc = start - 8;}
    else if (start >= 16 && start < 24 ){sr = 2;sc = start - 16;}
    else if (start >= 24 && start < 32 ){sr = 3;sc = start - 24;}
    else if (start >= 32 && start < 40 ){sr = 4;sc = start - 32;}
    else if (start >= 40 && start < 48 ){sr = 5;sc = start - 40;}
    else if (start >= 48 && start < 56 ){sr = 6;sc = start - 48;}
    else if (start >= 56 && start < 64 ){sr = 7;sc = start - 56;}
    
    //setting er and ec
    if(end >= 0 && end < 8){er = 0;ec = end;}
    else if(end >= 8 && end < 16){er = 1;ec = end - 8;}
    else if (end >= 16 && end < 24 ){er = 2;ec = end - 16;}
    else if (end >= 24 && end < 32 ){er = 3;ec = end - 24;}
    else if (end >= 32 && end < 40 ){er = 4;ec = end - 32;}
    else if (end >= 40 && end < 48 ){er = 5;ec = end - 40;}
    else if (end >= 48 && end < 56 ){er = 6;ec = end - 48;}
    else if (end >= 56 && end < 64 ){er = 7;ec = end - 56;}
    
    
    //checking if the move is up-right or up-left
    if(er == sr-1){
        if(ec == sc-1 || ec == sc+1){
            return true;
        }
    }
    
    //checking if the move is up-right jump or up-left jump
    if(er == sr-2){
        if(ec == sc - 2 || ec == sc + 2){
            return true;
        }
    }
    
    //checking if the move is down-right or down-left
    if(er == sr +1){
        if(ec == sc + 1 || ec == sc -1){
            return true;
        }
    }
    
    //chekcing if the move is down-right jump or down-left jump
    if(er == sr + 2){
        if(ec == sc - 2 || ec == sc + 2){
            return true;
        }
    }
    
    //if not any of the 8 possible valid moves then it will return false
    return false;
}

/*
 setOptions(const board&)
 param: board&
 use: it will go through the board and for each chip with the corresponding color
    to the computer it will go through and see if that piece has any valid moves 
    associated with it, is so it will be pushed back into moves
 */
void computer::setOptions(const board &b){
    int counter = 0;
    
    //clear all previous available moves
    while(moves.size()){
        moves.pop_back();
    }
    
    //loop through board
    while(counter < b.b.size()){
        
        //check if the correct color
        if(b.b[counter].getColor() == color){
            
            //starting location corresponds to the current value of the counter
            int start = counter;
            
            //check if down-left is a valid move
            if(b.b[start+7].getColor() == 'N'){
                if(checkEdgeCases(start, start+7, b)){
                    //if valid push move to moves
                    moves.push_back(options(start, start+7));
                }
            }
            
            //check if up-right is a valid move
            if(b.b[start-7].getColor() == 'N'){
                if(checkEdgeCases(start, start-7, b)){
                    //if valid push move to moves
                    moves.push_back(options(start, start-7));
                }
            }
            
            //check if down-left jump is a valid move
            if(b.b[start+14].getColor() == 'N'){
                if(b.b[start+7].colorIsSet() &&
                   b.b[start+7].color != color){
                    if(checkEdgeCases(start, start+14, b)){
                        //if valid push move to moves
                        moves.push_back(options(start, start+14));
                    }
                }
            }
            
            //check it up-right jump is a valid move
            if(b.b[start-14].getColor() == 'N'){
                if(b.b[start-7].colorIsSet() &&
                   b.b[start-7].color != color){
                    if(checkEdgeCases(start, start-14, b)){
                        //if valid push move to moves
                        moves.push_back(options(start, start-14));
                    }
                }
            }
            
            //check if down-right is a valid move
            if(b.b[start+9].getColor() == 'N'){
                if(checkEdgeCases(start, start+9, b)){
                    //if valid push move to moves
                    moves.push_back(options(start, start+9));
                }
            }
            
            //check if up-left if a valid move
            if(b.b[start-9].getColor() == 'N'){
                if(checkEdgeCases(start, start-9, b)){
                    //if valid push move to moves
                    moves.push_back(options(start, start-9));
                }
            }
            
            //check if down-right jump is a valid move
            if(b.b[start+18].getColor() == 'N'){
                if(b.b[start+9].colorIsSet() &&
                   b.b[start+9].color != color){
                    if(checkEdgeCases(start, start+18, b)){
                        //if valid push move to moves
                        moves.push_back(options(start, start+18));
                    }
                }
            }
            
            //check if up-left jump is a valid move
            if(b.b[start-18].getColor() == 'N'){
                if(b.b[start-9].colorIsSet() &&
                   b.b[start-9].color != color){
                    if(checkEdgeCases(start, start-18, b)){
                        //if valid push move to moves
                        moves.push_back(options(start, start-18));
                    }
                }
            }
        }
        
        //increment counter
        ++counter;
    }
}
