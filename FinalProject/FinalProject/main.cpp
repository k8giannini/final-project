//
//  main.cpp
//  Final Project
//
//  Created by Kate Giannini on 1/18/17.
//  Copyright © 2017 Kate Giannini. All rights reserved.
//

#include <iostream>
#include "Checkers_Classes.hpp"

using namespace std;

/*
 round_of_checkers(player*, player* , board&)
 param: player* , player* , board&
 use: executes a round of checkers as the name implies, returns true if round was
    not the end of the game, false if game ended during the round
 */
bool round_of_checkers(player* firstPlayer, player* secondPlayer, board& b){
    
    //check if there are still chips on the board
    if(!b.checkChips()){
        cout << "Game OVER!" << endl;
        return false;
    }
    
    //first player makes thier moves
    firstPlayer->play(b);
    b.setKings();
    b.display();
    
    //check if there are still chips on the board
    if(!b.checkChips()){
        cout << "Game OVER!" << endl;
        return false;
    }
    
    //second player makes thier move
    secondPlayer->play(b);
    b.setKings();
    b.display();
    
    //check if there are still chips on the board
    if(!b.checkChips()){
        cout << "Game OVER!" << endl;
        return false;
    }
    
    //board can still be played
    return true;
}

int main() {
    
    
    player* p1 = nullptr;
    player* p2 = nullptr;
    board b;
    
    //set up board
    int initRedChips[12] = {1,3,5,7,10,12,14,16,17,19,21,23};
    int initBlackChips[12] = {42,44,46,48,49,51,53,55,58,60,62,64};
    
    //set up red chips
    [&b, initRedChips](){
        size_t counter = 0;
        
        while(counter < 12){
            b.getChip(initRedChips[counter]-1).setColor('r');
            ++counter;
        }
    }();
    
    //set up black chips
    [&b, initBlackChips](){
        size_t counter = 0;
        while(counter < 12){
            b.getChip(initBlackChips[counter]-1).setColor('b');
            ++counter;
        }
    }();
    
    //set up players
    string name1;
    string name2;
    
    int playerCount = -1;
    
    cout << "Let's play Checkers! How many HUMAN players? " << endl;
    cin >> playerCount;
    
    //will loop until valid amount of players
    while(playerCount < 0 || playerCount > 2){
        cout << "Sorry I didn't understand. How many HUMAN players? " << endl;
        cin >> playerCount;
    }
    
    //will initialize player pointers
    switch(playerCount){
        case 0:
            p1 = new computer("Computer 1",'r');
            p2 = new computer("Computer 2",'b');
            break;
        case 1:
            cout << "Player, what is your name? " << endl;
            cin >> name1;
            p1 = new human(name1,'r');
            p2 = new computer("Computer",'b');
            break;
        case 2:
            cout << "Player 1 (Red), what is your name? " << endl;
            cin >> name1;
            cout << "Player 2 (Black), what is your name? " << endl;
            cin >> name2;
            p1 = new human(name1,'r');
            p2 = new human(name2,'b');
            break;
        default:
            p1 = nullptr;
            p2 = nullptr;
    }
    
    //check that pointer were initialized properly
    if(p1 == nullptr || p2 == nullptr){
        cout << "Sorry no checkers today :(" << endl;
        return -1;
    }
    
    //display the board
    b.display();
    
    //play checkers
    while(round_of_checkers(p1, p2, b)){}
    
}
